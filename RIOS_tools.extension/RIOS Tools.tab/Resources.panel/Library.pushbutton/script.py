#************************************************************************************
# Purpose:      Link to our in house revit family library.
#
# Inputs:       
#
# Returns:      Decribe any return values here if applicable.
# Title:        script.py
# Author:       Bob Frederick - bfrederick@rios.com
# Date Created: 2021-10-19 13:11:45
# TODO:         Describe anyto-do items here
#
#************************************************************************************

import webbrowser as wb

url = wb.open(r"\\rios.com\Shares\Resource\000 Models\Revit\03_Revit Library")
