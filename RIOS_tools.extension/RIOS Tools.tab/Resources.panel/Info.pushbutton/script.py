#************************************************************************************
# Purpose:      Introduce new users to RIOS_Tools for Revit.
#
# Inputs:       Example: width, height: the dimensions of the output image.
#
# Returns:      Decribe any return values here if applicable.
# Title:        module_name.py
# Author:       Author's Name & Email
# Date Created: Follow date format i.e. 7.31.2018
# TODO:         Describe anyto-do items here
#
#************************************************************************************

import webbrowser

url = 'https://loop.rchstudios.com/Technology/Wiki/RevitTools.aspx'

# Open URL in a new tab, if a browser window is already open.
webbrowser.open_new_tab(url)

# Open URL in new window, raising the window if possible.
webbrowser.open_new(url)