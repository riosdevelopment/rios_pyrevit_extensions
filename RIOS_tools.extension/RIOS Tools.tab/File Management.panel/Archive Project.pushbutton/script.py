# -*- coding: utf-8 -*-

#************************************************************************************
# Purpose:      Archive file to server directory.
#
# Inputs:       Example: Project Phase, Archive Path
#
# Returns:      Decribe any return values here if applicable.
# Title:        module_name.py
# Author:       Bob Frederick - bfrederick@rios.com
# Date Created: 2022-02-25 10:30:10
# TODO:         Describe anyto-do items here
#
#************************************************************************************

import os
from pyrevit import forms


# prompt user for phase
phases = ['07 CONCEPT DESIGN',
          '08 SCHEMATIC DESIGN',
          '09 DESIGN DEVELOPMENT',
          '10 CONSTRUCTION DOCUMENTS'
         ]
default = '07 CONCEPT DESIGN'
prompt = 'Select Project Phase'
title = 'Archive Project | Project Phase'
item = forms.ask_for_one_item(phases , default, prompt, title)
print(item)



